/*
BeaverBot discord bot built with JDA
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.beaverbot;

import java.util.Calendar;
import java.util.logging.Logger;

import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;

public class BusyBeaver extends Thread {

    private static final Logger logger = Logger.getLogger("BusyBeaver");

    private long lastAlertedMillis = 0;

    @Override
    public void run() {
        try {
            while(true) {
                Thread.sleep(BeaverConfig.getBoopIntervalSeconds() * 1000);

                if(lastAlertedMillis + BeaverConfig.getBoopWaitAfterAlertSeconds()*1000 > System.currentTimeMillis()) {
                    continue;
                }

                int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                int minute = Calendar.getInstance().get(Calendar.MINUTE);
                if((hour == 8 && minute >= 58) || (hour == 9 && minute <= 4)) {
                    continue;
                }

                String serverId = BeaverConfig.getBoopServerId();

                Guild server = BeaverBot.JDA.getGuildById(serverId);

                if(server == null) {
                    logger.severe("Could not find server by ID: " + serverId);
                    continue;
                }

                String boopId = BeaverConfig.getBoopId();

                Member boop = server.retrieveMemberById(boopId).useCache(false).complete();

                if(boop == null) {
                    logger.severe("Could not find boop in server by ID: " + boopId);
                    continue;
                }

                if(boop.getOnlineStatus() != OnlineStatus.ONLINE) {
                    logger.info("Boop is down! :(((((((((");
                    for(String boyId : BeaverConfig.getBoopBoys()){
                        Member boy = server.retrieveMemberById(boyId).useCache(false).complete();
                        if(boy == null){
                            logger.severe("Could not find boy by ID: " + boyId);
                            continue;
                        }
                        PrivateChannel channel = boy.getUser().openPrivateChannel().useCache(false).complete();
                        if(channel == null){
                            logger.severe("Could not open private channel to boy with ID: " + boyId);
                            continue;
                        }
                        logger.info("Sending message to boy " + boy.getEffectiveName());
                        channel.sendMessage("Ketté van dögölve a szerver pls fix.").complete();
                    }
                    lastAlertedMillis = System.currentTimeMillis();
                }
            }
        } catch (InterruptedException e) {
            logger.info("BusyBeaver thread interrupted. Stopping.");
        }
    }
}
