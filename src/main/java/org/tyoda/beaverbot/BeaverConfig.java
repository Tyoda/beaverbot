/*
BeaverBot discord bot built with JDA
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.beaverbot;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

public class BeaverConfig {

    public static final String CONFIG_FILE_MESSAGE = " Konfig beaverbot";
    public static final String CONFIG_TOKEN_NAME = "bot_token";
    public static final String CONFIG_SCREENSHOTS_NAME = "screenshots_channel";
    public static final String CONFIG_BOOP_SERVER_ID_NAME = "boop_server_id";
    public static final String CONFIG_BOOP_ID_NAME = "boop_id";
    public static final String CONFIG_BOOP_BOY_IDS_NAME = "boop_boys";
    public static final String CONFIG_BOOP_INTERVAL_SECONDS_NAME = "boop_interval_seconds";
    public static final String CONFIG_BOOP_WAIT_SECONDS_NAME = "boop_wait_after_alert_seconds";

    private static String screenshotsChannelName = "screenshots";
    private static String token = "";
    private static String boopServerId = "";
    private static String boopId = "";
    private static List<String> boopBoys = new ArrayList<>();
    private static int boopIntervalSeconds = 5 * 60;
    private static int boopWaitAfterAlertSeconds = 120 * 60;

    public static void configure(String configFilePath) {
        if(new File(configFilePath).exists()) {
            try(InputStream is = Files.newInputStream(Paths.get(configFilePath))) {
                Properties config = new Properties();
                config.load(is);
                token = config.getProperty(CONFIG_TOKEN_NAME, token).strip();
                screenshotsChannelName = config.getProperty(CONFIG_SCREENSHOTS_NAME, screenshotsChannelName).strip();
                boopServerId = config.getProperty(CONFIG_BOOP_SERVER_ID_NAME, boopServerId).strip();
                boopId = config.getProperty(CONFIG_BOOP_ID_NAME, boopId).strip();
                boopIntervalSeconds = Integer.parseInt(config.getProperty(CONFIG_BOOP_INTERVAL_SECONDS_NAME, String.valueOf(boopIntervalSeconds)).strip());
                boopWaitAfterAlertSeconds = Integer.parseInt(config.getProperty(CONFIG_BOOP_WAIT_SECONDS_NAME, String.valueOf(boopWaitAfterAlertSeconds)).strip());
                for(String boy : config.getProperty(CONFIG_BOOP_BOY_IDS_NAME, "").split(",")) {
                    String boyStripped = boy.strip();
                    if(boyStripped.isEmpty())
                        continue;
                    boopBoys.add(boyStripped);
                }
            } catch (IOException e) {
                BeaverBot.logger.log(Level.SEVERE, "Failed while opening config file to read.", e);
                throw new RuntimeException(e);
            }
        } else {
            try(OutputStream os = Files.newOutputStream(Paths.get(configFilePath))) {
                Properties defaultConfig = new Properties();
                defaultConfig.put(CONFIG_TOKEN_NAME, "<YOUR_TOKEN>");
                defaultConfig.put(CONFIG_SCREENSHOTS_NAME, "screenshots");
                defaultConfig.put(CONFIG_BOOP_SERVER_ID_NAME, "<BOOP_SERVER_ID>");
                defaultConfig.put(CONFIG_BOOP_ID_NAME, "<BOOP_ID>");
                defaultConfig.put(CONFIG_BOOP_BOY_IDS_NAME, "<BOOP_BOY_ID_1> , <BOOP_BOY_ID_2>");
                defaultConfig.put(CONFIG_BOOP_INTERVAL_SECONDS_NAME, String.valueOf(boopIntervalSeconds));
                defaultConfig.put(CONFIG_BOOP_WAIT_SECONDS_NAME, String.valueOf(boopWaitAfterAlertSeconds));
                defaultConfig.store(os, CONFIG_FILE_MESSAGE);
            } catch (IOException e) {
                BeaverBot.logger.log(Level.SEVERE, "Failed while opening config file to write.", e);
                throw new RuntimeException(e);
            }
        }
    }

    public static String getScreenshotsChannelName() {
        return screenshotsChannelName;
    }

    public static String getToken() {
        return token;
    }

    public static String getBoopServerId() {
        return boopServerId;
    }

    public static String getBoopId() {
        return boopId;
    }

    public static int getBoopIntervalSeconds() {
        return boopIntervalSeconds;
    }

    public static List<String> getBoopBoys() {
        return new ArrayList<>(boopBoys);
    }

    public static int getBoopWaitAfterAlertSeconds() {
        return boopWaitAfterAlertSeconds;
    }
}
