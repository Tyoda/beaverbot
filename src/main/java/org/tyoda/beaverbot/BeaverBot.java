/*
BeaverBot discord bot built with JDA
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.beaverbot;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.channel.concrete.ThreadChannel.AutoArchiveDuration;
import net.dv8tion.jda.api.entities.channel.unions.MessageChannelUnion;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

public class BeaverBot extends ListenerAdapter {
    
    public static final Logger logger = Logger.getLogger("BeaverBot");

    public static final String CONFIG_FILE_PATH = "beaverbot.config";

    public static JDA JDA = null;

    public static void main(String[] args) {

        BeaverConfig.configure(CONFIG_FILE_PATH);

        if(BeaverConfig.getToken().isEmpty()) {
            logger.severe("Could not find token. Exiting.");
            return;
        }

        JDA = JDABuilder.createDefault(BeaverConfig.getToken())
                .enableIntents(GatewayIntent.MESSAGE_CONTENT, GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_PRESENCES)
                .setMemberCachePolicy(MemberCachePolicy.ALL)
                .enableCache(CacheFlag.ONLINE_STATUS)
                .build();

        JDA.addEventListener(new BeaverBot());

        new BusyBeaver().start();
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        User author = event.getAuthor();
        if(author.isBot()) {
            return;
        }

        Message message = event.getMessage();
        MessageChannelUnion channel = message.getChannel();
        String text = message.getContentRaw();

        if(channel.getName().equals(BeaverConfig.getScreenshotsChannelName()) && (!message.getAttachments().isEmpty() || hasLink(text))) {
            String threadName = author.getEffectiveName();
            if (!text.isEmpty() && !text.startsWith("http")) {
                if(text.length() <= 20) {
                    threadName = text;
                } else {
                    threadName = text.substring(0, 20) + "...";
                }
            }

            logger.info("Creating new thread: " + threadName);

            message.createThreadChannel(threadName)
                    .reason("You may compliment or talk about the message here")
                    .setAutoArchiveDuration(AutoArchiveDuration.TIME_3_DAYS)
                    .queue(null, (e) -> { logger.log(Level.SEVERE, "Failed to create thread in screenshots.", e); });
        }
    }

    public static boolean hasLink(String message) {
        return Stream.of(message.split("\\s")).anyMatch(x -> x.startsWith("http"));
    }
}